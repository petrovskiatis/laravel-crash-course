@props(['post' => $post])

<div class="mb-4">
    <a href="{{ route('user.posts', $post->user) }}" class="font-bold"> {{ $post->user->name }} <span class="text-gray-600">{{ $post->created_at->diffForHumans() }}</span></a>
    <p class="mb-2">{{ $post->body }}</p>
</div>
@can('delete', $post)
    <form action="{{ route('posts.destroy', $post) }}" method="post" class="mr-1">
        @csrf
        @method('DELETE')
        <button class="text-blue-500">Delete</button>
    </form>
@endcan
<div class="flex items-center">
    @if (!$post->likedBy(auth()->user()))
        <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">
            @csrf
            <button class="text-blue-500">Like</button>
        </form>
    @else
        <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="text-blue-500">Unlike</button>
        </form>
    @endif
    <span>{{ $post->likes->count()  }} {{ Str::plural('like', $post->likes->count()) }}</span>
</div>
