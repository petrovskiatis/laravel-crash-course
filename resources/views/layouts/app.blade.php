<!doctype html>
<head>
    <title>Posty</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
</head>
<body class='bg-gray-200'>
    <nav class='p-6 bg-white flex justify-between mb-6'>
        <ul class='flex items-center'>
            <li class='p-3'>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li class='p-3'>
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class='p-3'>
                <a href="{{ route('posts') }}">Post</a>
            </li>
        </ul>

        <ul class='flex items-center'>
            @auth
            <li>
                <a class='p-3' href="">
                    Blagoja Petrovski
                </a>
            </li>
            <li>
                <form action="{{ route('logout') }}" method='post' class='inline'>
                    @csrf
                    <button type='submit' >Logout</button>
                </form>
            </li>
            @endauth

            @guest
             <li>
                <a class='p-3' href="{{ route('login') }}">Login</a>
            </li>
            <li>
                <a class='p-3' href="{{ route('register') }}">Register</a>
            </li>
            @endguest
        </ul>

    </nav>
    @yield('content')
</body>
</html>
