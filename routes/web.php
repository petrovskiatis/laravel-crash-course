<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/users/{user:username}/posts', [\App\Http\Controllers\UserPostController::class, 'index'])->name('user.posts');

Route::get('/dashboard', [DashboardController::class, 'index'])
        ->name('dashboard');

Route::post('/logout', [LogoutController::class, 'store'])->name('logout');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'store']);

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/posts', [\App\Http\Controllers\PostController::class, 'index'])->name('posts');
Route::get('/posts/{post}', [\App\Http\Controllers\PostController::class, 'show'])->name('posts.show');
Route::post('/posts', [\App\Http\Controllers\PostController::class, 'store']);
Route::delete('/posts{post}', [\App\Http\Controllers\PostController::class, 'destroy'])->name('posts.destroy');

Route::post('/posts//{post}/likes', [\App\Http\Controllers\PostLikeController::class, 'store'])->name('posts.likes');
Route::delete('/posts//{post}/likes', [\App\Http\Controllers\PostLikeController::class, 'destroy'])->name('posts.likes');
